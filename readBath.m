function [ data ] = readBath( filenameIn )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
try
    fileIdIn = fopen(filenameIn,'r','ieee-be');
    nodeHeader = int32(fscanf(fileIdIn,'%d %d %d ',[3,1]));
    data.proj = fscanf(fileIdIn,'%s\n',[1,1]);
    data.nBNodes = nodeHeader(3);
    data.bNodeRefs = int32(zeros(data.nBNodes,1));
    data.bNodeCoords = zeros(data.nBNodes,3);
    data.bNodeCode = int32(zeros(data.nBNodes,1));
    for nodeIndex = 1:1:data.nBNodes
        data.bNodeRefs(nodeIndex)=int32(fscanf(fileIdIn,'%d ',[1,1]));
        data.bNodeCoords(nodeIndex,:)=fscanf(fileIdIn,'%f',[1,3]);
        data.bNodeCode(nodeIndex)=int32(fscanf(fileIdIn,'%d',[1,1]));
        fscanf(fileIdIn,'\n');
    end
    elemHeader = int32(fscanf(fileIdIn,'%d %d %d\n',[3,1]));
    data.bNElem = elemHeader(1);
    data.bNodesPerElem = elemHeader(2);
    data.bElemRefs = int32(zeros(data.nBNodes,1));
    data.bElemDefs = int32(zeros(data.nBNodes,data.bNodesPerElem));
    for elemIndex = 1:1:data.bNElem
        data.bElemRefs(elemIndex) = int32(fscanf(fileIdIn,'%d',[1,1]));
        data.bElemDefs(elemIndex,:) = int32(fscanf(fileIdIn,'%d',[1,data.bNodesPerElem]));        
        fscanf(fileIdIn,'\n');
    end
    fclose(fileIdIn);
catch exp
    fclose(fileIdIn);
    rethrow(exp);
end

