function [ data ] = readFlowV1( filenameIn,varargin)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here
v=int64(1);
tagsFilename = sprintf('tagsV%d.mat',v);
load(tagsFilename);
assert(v==VTAG);

nVarargs = length(varargin);
if(nVarargs ~= 0)
    data = varargin{1};
end

try
    fileIdIn = fopen(filenameIn,'r','ieee-be');
    readTag(fileIdIn,HEADERSTART);
        readTag(fileIdIn,VERSION);
        data.version = fread(fileIdIn, 1, '*int64');
        assert(data.version == v);
        readTag(fileIdIn,TIMESTART);
            readTag(fileIdIn,DELTAT);
            data.deltat = fread(fileIdIn, 1, 'double');
            readTag(fileIdIn,NTIMESTEPS);
            data.nTimeSteps = fread(fileIdIn, 1, '*int32');
        readTag(fileIdIn,TIMEEND);
        readTag(fileIdIn,ELESTART);
            readTag(fileIdIn,NBNODES);
            data.nBNodes = fread(fileIdIn, 1, '*int32');
            readTag(fileIdIn,BNODEREFS);
            data.bNodeRefs = fread(fileIdIn, data.nBNodes, '*int32');
        readTag(fileIdIn,ELEEND);
        readTag(fileIdIn,LAYERSSTART);
            readTag(fileIdIn,NLAYERS);
            data.nLayers = fread(fileIdIn, 1, '*int32');
            readTag(fileIdIn,SIGMADEPTHS);
            data.sigmaDepths = fread(fileIdIn, data.nLayers, 'double');
        readTag(fileIdIn,LAYERSEND);
        readTag(fileIdIn,VALSSTART);
            readTag(fileIdIn,VALSPERFNODE);
            data.valsPerFNode = fread(fileIdIn, 1, '*int32');
        readTag(fileIdIn,VALSEND);
    readTag(fileIdIn,HEADEREND);
        data.uv = zeros(data.valsPerFNode,data.nLayers,data.nBNodes,data.nTimeSteps);
    readTag(fileIdIn,DATASTART);
    for timeIndex = 1:1:data.nTimeSteps
        readTag(fileIdIn,FRAMESTART);
        for nodeIndex = 1:1:data.nBNodes
            for layerIndex = 1:1:data.nLayers
                for valIndex = 1:1:data.valsPerFNode
                    data.uv(valIndex,layerIndex,nodeIndex,timeIndex)=fread(fileIdIn, 1, 'double');
                end
            end
        end
        readTag(fileIdIn,FRAMEEND);
    end
    readTag(fileIdIn,DATAEND);
    fclose(fileIdIn);
catch exp
    fclose(fileIdIn);
    rethrow(exp);
end


end

