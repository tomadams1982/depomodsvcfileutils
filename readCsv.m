function [data] = readCsv(fileIdIn,data,field)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here
s1 = {['#',data.startOfDataMarker],['#',data.endOfDataMarker]};
count=0;
    while ~feof(fileIdIn)
        token =  fscanf(fileIdIn,'%s\n',[1,1]);
        if(sum(strcmp(s1,token))~=1)
            csv = strsplit(token,',');
            count=count+1;
            for i=1:1:length(csv)
                eval(sprintf('data.%s(%d,%d)=double(%s);',field,count,i,csv{i}));
            end
        end
    end
    eval(sprintf('data.count=%d;',count));
end
