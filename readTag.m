function [ tag ] = readTag( fileIdIn,tagConstant)
tag = fread(fileIdIn, 1, '*int64');
assert(tag == tagConstant);
end

