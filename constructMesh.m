%read nodes
nodesFileID=fopen('C:\Users\sa01tc\Desktop\testData\bathymetry\XyzMikeMeshNodes.csv');
mesh.startOfDataMarker='startOfDataMarker';
mesh.endOfDataMarker='endOfDataMarker';
mesh=readCsv(nodesFileID,mesh,'nodeRefs');
fclose(nodesFileID);
%read triangles
trianglesFileID=fopen('C:\Users\sa01tc\Desktop\testData\bathymetry\XyzMikeMeshTriangles.csv');
mesh=readCsv(trianglesFileID,mesh,'triangleRefs');
fclose(trianglesFileID);

%process
%delete dry interior nodes
[nodRef,J]=find(mesh.nodeRefs(:,5)==-1);
for nodRefIndex=1:1:length(nodRef)
    for nodeIndex=2:1:4
        [triRef,J]=find(mesh.triangleRefs(:,nodeIndex)==nodRef(nodRefIndex));
        mesh.triangleRefs(triRef,:)=[];
    end
end
mesh.nodeRefs(nodRef,:)=[];

%set dry nodes to z==10
[nodRef,J]=find(mesh.nodeRefs(:,5)==1);
mesh.nodeRefs(nodRef,4)=10;

%set wet nodes with z>-5 to z==-5
[nodRef,J]=find(mesh.nodeRefs(:,4)>-5 & mesh.nodeRefs(:,4)<0);
mesh.nodeRefs(nodRef,4)=-5;

%write mike mesh
mesh.crs='PROJCS["British_National_Grid",GEOGCS["GCS_OSGB_1936",DATUM["D_OSGB_1936",SPHEROID["Airy_1830",6377563.396,299.3249646]],PRIMEM["Greenwich",0],UNIT["Degree",0.017453292519943295]],PROJECTION["Transverse_Mercator"],PARAMETER["False_Easting",400000],PARAMETER["False_Northing",-100000],PARAMETER["Central_Meridian",-2],PARAMETER["Scale_Factor",0.999601272],PARAMETER["Latitude_Of_Origin",49],UNIT["Meter",1]';
mesh.fileName='C:\Users\sa01tc\Desktop\testData\bathymetry\somewhereOnSeaSVC.depomodbathymetrymikemesh';
writeMikeMesh(mesh);
