Spatially varying currents in NewDEPOMOD

Tom Adams 

Scottish Association for Marine Science

5th June 2019

Summary

When modelling the spread of particulate matter from fish farm cages, models (including AutoDEPOMOD and NewDEPOMOD) have generally used flow data obtained from a single spatial point current meter reading. In Scotland this current meter reading is obtained in accordance with the requirements of the regulator, SEPA. This single point value is applied throughout the domain, giving, in effect, a spatially homogeneous current field that ignores any variation in flow in different locations. This is not a realistic representation of flow over the whole spatial domain, particularly in areas with complex bathymetry or coastline.
The advent of detailed and accurate hydrodynamic models for coastal areas means that current fields more accurately representing this spatial variation in flow patterns are now available throughout the regions occupied by Scottish salmon farming operations. Such data sets could theoretically be generated using concurrent multiple current meter deployments, though this would generally be prohibitively expensive.
NewDEPOMOD now incorporates the capability to read and apply spatially variable current fields when modelling spread and deposition of particulate matter. This section describes the process of data preparation using freely available Matlab scripts, and how models are run once the data have been prepared.

Data file preparation

In order to make a NewDEPOMOD simulation based on spatially varying current fields, two files are required in a different format to those normally used.

Bathymetry file

A bathymetry file describing the spatial structure of the model domain (and the corresponding flowmetry file) is required. The format for this file is similar to the MIKE .mesh format. The file consists of:
- A header line containing three numbers (the third of which must be the number of bathymetry nodes) and a string detailing the spatial projection of the bathymetry mesh. Example:
100079 1000 2906 PROJCS["British_National_Grid",GEOGCS["GCS_OSGB_1936",DATUM["D_OSGB_1936",SPHEROID["Airy_1830",6377563.396,299.3249646]],PRIMEM["Greenwich",0],UNIT["Degree",0.017453292519943295]],PROJECTION["Transverse_Mercator"],PARAMETER["False_Easting",400000],PARAMETER["False_Northing",-100000],PARAMETER["Central_Meridian",-2],PARAMETER["Scale_Factor",0.999601272],PARAMETER["Latitude_Of_Origin",49],UNIT["Meter",1]
The first two numbers are not used, the third is the number of model nodes.
- A line for each bathymetry node, each containing the node ID number, x-coordinate, y-coordinate, depth, and node type. Example
1 142266 780643 -8.83552 2

The node type number should be:
- 0 for all "interior" nodes i.e. those not on a boundary
- 1 for all nodes on a hard (land) boundary
- 2 (or higher) for all nodes on an open (water) boundary. Note that continuous "strings" of open nodes should have an identical number, and separate sections of open boundary should have different node type numbers.

The number of such lines should be the same as the number of nodes identified in item 3 of the first line.
- A line stating the number of triangular elements to be formed from the provided node locations, and two extra numbers that are not used presently. Example:
5587  3  21
- A line for each triangular mesh element. listing the element ID number, and ID number of the three nodes composing it (listed in anticlockwise order). Example:
105 55 79 75

The total number of lines in the file should therefore be 1 + Nnodes + 1 + Nelements

Each entry should be separated by a space.

The created file should have the extension ".depomodbathymetrymikemesh".

Flowmetry file

A flowmetry file corresponding to the bathymetry file structure must be provided in binary format. The binary file can be constructed in Matlab using routines authored by Trevor Carpenter (https://bitbucket.org/tomadams1982/depomodsvcfileutils).

The first step is to prepare a Matlab structure (referred to as "data" below) containing the following fields:
- originTime (the epoch at the start of the period of model output; https://www.epochconverter.com/)
- startTime (the epoch at the start of the file output, in this case the same as originTime; https://www.epochconverter.com/)
- deltat (the timestep used in the current fields, in seconds)
- nTimeSteps (number of timesteps in current fields)
- nBNodes (the number of surface boundary nodes)
- bNodeRefs (ID numbers for surface boundary nodes, corresponding to the node IDs in the bathymetry file); a list of integers 1 to nBNodes
- nLayers (the number of sigma (depth) layers in the current field)
- sigmaDepths (the proportion of water depth at which the currents are reported, from 0 to -1); length=nLayers
- valsPerBNode (the number of values on each bNode); normally=1
- bNodeValNames (numeric names of bNode fields in base 36 representation, obtained from dictionaryV2.mat); e.g M.bNodeValNames= DICTIONARY.BNODEVALS.ELEVATION; https://en.wikipedia.org/wiki/Base36
- bNodeVals (surface boundary values: elevation); size: valsPerBNode x nBNodes x nTimeSteps
- valsPerFNode (the number of values on each fNode); up to 5
- fNodeValNames (names of fNode fields in base 36 representation, obtained from dictionaryV2.mat); e.g 
    	M.fNodeValNames = ...
        cat(1, DICTIONARY.FNODEVALS.UVELOCITY,...
        DICTIONARY.FNODEVALS.VVELOCITY, ...
        DICTIONARY.FNODEVALS.WVELOCITY,  ...
        DICTIONARY.FNODEVALS.TEMPERATURE, ...
        DICTIONARY.FNODEVALS.SALINITY );
- fNodeVals (field values: s, t, u, v, w); size: valsPerFNode x nLayers x nBNodes x nTimeSteps

Bnodes data is recorded at surface points only (size: valsPerBNode x nBNodes x nTimeSteps), and Fnode data is recorded at all 3D data points, that is at all depths (size: valsPerFNode x nLayers x nBNodes x nTimeSteps). NewDEPOMOD expects the bottom layer of the supplied fields ("-1" depth) to be zero everywhere.
Running the Matlab script writeFlowV2.m enables the generation of a binary file in the required format. The created file should have the extension ".depomodflowmetrybinary", for example:
writeFlowV2(data,'MySite.depomodflowmetrybinary')

Model operation - configuration

For runs making use of spatially varying currents, the amount of data read in is much greater than for a standard "single current meter" run. Therefore, it is recommended to increase the amount of RAM available to NewDEPOMOD when it runs.
This may be acheived by editing the files:

C:\Program Files\depomoduserinterface\etc\depomoduserinterface.conf
C:\Program Files\depomodruntimecontainer\etc\depomodruntimecontainer.conf

These files provide configuration properties for the user interface and the command line versions of the model, respectively. You will need administrator rights to edit these files.
Line 7 of each file tells DEPOMOD how much memory to make available (in megabytes). 

default_options="--branding depomoduserinterface -J-Xms2048m -J-Xmx4096m"

It is recommended to set the starting memory to at least 2 GB (2048 MB), and the full available memory to up to 50% of what is available on your system (for example, on an 8 GB system, setting the second value to 4 GB (4096 MB), as above).

Model operation - project setup

In order to create a NewDEPOMOD project that implements spatially variable currents, the procedure is slightly different to a standard project. The steps are as follows:
1.	Create a new NewDEPOPMOD project with the same command as normal from the "File" menu in the interface (or manually by creating a directory structure using your preferred method).
2.	Place the created bathymetry file into the "bathymetry" subdirectory of the project folder, ensuring that the main part of the file name matches the name of the NewDEPOMOD project.
3.	Place the created flowmetry file into the "flowmetry" subdirectory of the project folder, again ensuring that the main part of the file name matches the name of the NewDEPOMOD project.
4.	In the file models/MODELNAME.depomodconfigurationproperties, change:
a.	bathymetry.data.extension=depomodbathymetryproperties to bathymetry.data.extension=depomodbathymetrymikemesh
b.	flowmetry.data.extension=depomodflowmetryproperties to flowmetry.data.extension=depomodflowmetrybinary

User interface

To run the project in the user interface:
1.	Reopen the project in the user interface, create cages and inputs, and change all other settings as usual. The bathymetry can be plotted on the interface, but double clicking the flowmetry file name currently returns nothing.
2.	Run the model as usual by right clicking the relevant model under "models", and select Single or Optimise Model Run.

Command line

Once the project has been configured and the files added, the process of running a project that implements spatially variable currents from the command line is identical to the standard case.
